# -*- coding: utf-8 -*-
"""
    concurrency
    ~~~~~~~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import logging
import logging.handlers

import collections
import json
import re
import sys
import time
import multiprocessing

import zmq

DEFAULT_CONNECT_STRING = "tcp://127.0.0.1:5556"

class Object(object):
    """ 
    All classes should subclass this base class.
    """
    
    # The log format defines the way that logged messaged are displayed by the
    # logging handler. The current format will result in the following sequence:
    #   1. asctime,
    #   2. log level name,
    #   3. id of the logger's owner object,
    #   4. process ID (if available),
    #   5. log message.
    # @TODO: add class name here. But how does one get it?
    LOG_FORMAT = "%(asctime)s [%(levelname)s] [%(name)s] " + \
                 "[%(process)d]: %(message)s"

    # Defines the log level for all Pierre objects.
    LOG_LEVEL  = logging.DEBUG

    def __init__(self):
        pass

    def __repr__(self):
        if not hasattr(self,"__unicode__"):
            return u"%s:%s" % (self.__class__,id(self))
        else:
            return self.__unicode__().encode('utf-8')

    @property
    def logger(self):
        """
        Every 'PierreObject' has access to an internal logger. Logging is
        capsuled within the objects themselves because of concurrency issues.
        The current logging configuration logs to syslog (also because of
        concurrency issues with using stdin/stdout/stderr.
        """
        if not hasattr(self,"_logger"):
            self._logger = logging.getLogger(str(id(self)))

            # Log level
            self._logger.setLevel(self.__class__.LOG_LEVEL)

            # SysLog handler
            sysLogHandler = logging.handlers.SysLogHandler(address="/dev/log")
            # Handler format
            frmt = "{" + self.__class__.__name__ + "} " +\
                    self.__class__.LOG_FORMAT
            logFormatter = logging.Formatter(frmt)
            sysLogHandler.setFormatter(logFormatter)
            self._logger.addHandler(sysLogHandler)

        return self._logger

# ZMQ send and receive functions. These depend on the transport type and should
# not be changed!
_ZMQSend = zmq.Socket.send_json
_ZMQRecv = zmq.Socket.recv_json

class _ZMQContextPropertyMixin(object):
    """
    This mixin provides a thread- and process-safe ZMQ context. A _zmqContext
    property is defined and can then be accessed in the subclass. It should be
    noted, that the ZMQ context can not be created before a process forks as is
    is not passed to the child process. This mixin provides functionality for 
    avoiding this situation. If the init function's checkIsAlive attribute is 
    set to True during instantiation, a pre-fork property access will raise a 
    RuntimeError. It is a good idea to enable the pre-fork check.
    """
    def __init__(self,checkIsAlive=False):

        self.checkIsAlive = checkIsAlive

        # Internal context (should never be accessed internally).
        self._zmqContextInternal = None

    @property
    def _zmqContext(self):
        if self.checkIsAlive and not self.is_alive():
            raise RuntimeError("ZMQ context cannot be accessed before "\
                             + "the process forks.")
        if self._zmqContextInternal is None:
            # Create the context
            self._zmqContextInternal = zmq.Context()
        return self._zmqContextInternal

class _ZMQSocketPropertyMixin(_ZMQContextPropertyMixin):
    """
    This mixin provides a thread- and process-safe ZMQ socket. A _zmqSocket
    property is defined and can then be accessed in the subclass. It should be
    noted, that the ZMQ socket can not be created before a process forks as is
    is not passed to the child process. This mixin provides functionality for 
    avoiding this situation. If the init function's checkIsAlive attribute is 
    set to True during instantiation, a pre-fork property access will raise a 
    RuntimeError. It is a good idea to enable the pre-fork check.
    """
    def __init__(self,socketType,checkIsAlive=False):

        _ZMQContextPropertyMixin.__init__(self,checkIsAlive)

        self.socketType = socketType

        self.checkIsAlive = checkIsAlive

        # Internal socket (should never be accessed externally).
        self._zmqSocketInternal = None

    @property
    def _zmqSocket(self):
        if self.checkIsAlive and not self.is_alive():
            raise RuntimeError("ZMQ socket cannot be accessed before "\
                             + "the process forks.")
        # Create the socket if it has not yet been created.
        if self._zmqSocketInternal is None:
            # Create the socket
            self._zmqSocketInternal = self._zmqContext.socket(self.socketType)
        return self._zmqSocketInternal

class Process(multiprocessing.Process,Object):
    """
    This class provides a multiprocessing.Process based process class with
    built-in syslog logging support (as it subclasses Object). It also
    multiprocessing.Process's run method, meaning that the run method should
    not be overriden in subclasses; instead a loop method should be defined
    with the process' logic (the process stops when the loop method returns).
    Additionaly a method named 'after_fork_init' can be defined. If this method
    exists it will be called before the process forks.
    """
    def __init__(self):
        super(Process,self).__init__()
    def run(self):
        return self.loop()

class ZMQProcess(Process):
    def __init__(self):
        super(ZMQProcess,self).__init__()
    
    def _send(self,request,*args,**kwargs):
        if request is None:
            request = FailureResponse("_send received None response!")
        rq = request.to_dict()
        _ZMQSend(self._zmqSocket,rq,*args,**kwargs)

    def _recv(self,*args,**kwargs):
        rp = _ZMQRecv(self._zmqSocket,*args,**kwargs)
        if rp is None:
            raise ValueError("bad type `%s' for request/response" % type(rp))
        if "type" not in rp:
            raise ValueError("no type attribute in request/response dict")
        type_ = rp["type"]
        del(rp["type"])
        if hasattr(sys.modules[__name__],type_):
            return getattr(sys.modules[__name__],type_).from_dict(rp)
        else:
            raise ValueError("unknown type `%s' of request/response" % type_)



class ServerProcess(ZMQProcess,_ZMQSocketPropertyMixin):
    """
    This class provides a basic ZMQ based server process. On process start the
    server enters a blocking request/response loop, meaning that it will wait
    until it receives a request, then wait until the response is computed and
    then reply with that response.

    For specific server implementations the respond method must be overwritten
    (see its own documentation).
    """
    def __init__(self,connectString=DEFAULT_CONNECT_STRING,ignoreExceptions=True):
        super(ServerProcess,self).__init__()
        _ZMQSocketPropertyMixin.__init__(self,\
                socketType=zmq.REP,\
                checkIsAlive=True)
        
        self.ignoreExceptions = ignoreExceptions
        self.connectString = connectString
        self.running = False

        self.logger.info("Intializing at: %s" % self.connectString)

    def loop(self):
        """
        Server request/response loop (blocking).
        """
        # Binding socket
        self._zmqSocket.bind(self.connectString)

        self.logger.debug("Entering request/response loop.")

        self.running = True
        while self.running:
            request = self._recv()
            try:
                response = self._respond(request)
            except Exception as e:
                if self.ignoreExceptions:
                    self.logger.error("Internal server error: `%s'" % str(e))
                    return FailureResponse("internal server error: `%s'" % str(e))
                else: raise e
            self._send(response)

    def _respond(self,request):
        self.logger.debug("Received request: %s." % str(request))
        response = self.respond(request)
        self.logger.debug("Replying with response: %s." % str(response))
        return response
    
    def respond(self,request):
        """
        The server loop's response method. This should be overwritten by a
        subclass. It receives the request bytes, JSON or Python object and
        should return a response in a format that matches the ZMQ transport
        types send and recv functions.
        """
        raise NotImplementedError("must subclass!")

class ClientProcess(ZMQProcess,_ZMQSocketPropertyMixin):
    """
    This class provides a basic ZMQ based client process. The request method can
    be called with a request object (which must match the ZMQ transport layer's
    send and recv functions). This request is sent to the server and the process
    then blocks until a response is received.
    """
    def __init__(self,connectString=DEFAULT_CONNECT_STRING,local=False):
        super(ClientProcess,self).__init__()
        _ZMQSocketPropertyMixin.__init__(self,\
                socketType=zmq.REQ,\
                checkIsAlive=(not local))

    def _request(self,server,request):
        self._zmqSocket.connect(server)
        self.logger.debug("Sending request: %s." % str(request))
        self._send(request)
        response = self._recv()
        self.logger.debug("Received response: %s." % str(response))
        return response

    def request(self,server,request):
        """
        The client's request method. It takes a request and (via the _request
        method) then sends this request to the server and blocks until it 
        receives a response. The response is then returned to the called.
        """
        return self._request(server,request)

# ------------------------------------------------------------------------------
# Server Implementations

class EchoServer(ServerProcess):
    def __init__(self,connectString=DEFAULT_CONNECT_STRING,ignoreExceptions=True):
        super(EchoServer,self).__init__(connectString,ignoreExceptions)

    def respond(self,request):
        return SuccessResponse(data=request.data)

class DataStorageServer(ServerProcess):
    def __init__(self,connectString=DEFAULT_CONNECT_STRING,ignoreExceptions=True):
        super(DataStorageServer,self).__init__(connectString,ignoreExceptions)
        self.data = {}

    def respond(self,request):
        if request.method == "GET":
            try:
                data = self.data[request.location]
                return SuccessResponse(data=data)
            except KeyError:
                return FailureResponse(error=("unknown data location %s" 
                    % request.location))
        elif request.method == "SET":
            self.data[request.location] = request.data
            return SuccessResponse()
        else:
            return FailureResponse("unknown request method")

# ------------------------------------------------------------------------------
# Requests/Responses

def to_dict(obj):
    return obj.__dict__
def from_dict(cls,dict_):
    return cls(**dict_)

class Message(object):
    def __init__(self,_type):
        self.type = _type

class Request(Message):
    from_dict = classmethod(from_dict)
    to_dict = to_dict
    def __init__(self,method,location,data=None,extra=None):
        super(Request,self).__init__("Request")
        self.method = method
        self.location = location
        self.data = data
        self.extra = extra

    def __repr__(self):
        return ("METHOD: {meth}\nLOCATION: {loc}\nDATA: {data}\nEXTRA: {extra}"+\
                "\n").format(meth=self.method,loc=self.location,data=self.data,
                        extra=self.extra)

class SuccessResponse(Message):
    from_dict = classmethod(from_dict)
    to_dict = to_dict
    def __init__(self,data=None):
        super(SuccessResponse,self).__init__("SuccessResponse")
        self.data = data

    def __repr__(self):
        return "*** SuccessResponse ***\nData: {data}\n".format(data=self.data)

class FailureResponse(Message):
    from_dict = classmethod(from_dict)
    to_dict = to_dict
    def __init__(self,error=None):
        super(FailureResponse,self).__init__("FailureResponse")
        self.error = error

    def __repr__(self):
        return "*** FailureResponse ***\nError: {err}\n".format(err=error)
